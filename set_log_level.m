function set_log_level(level) 
% Sets the global logging level to one of 'none', 'off' (synonyms), 'info', 'verbose', or 'debug'
    
    % Required global variables
    global STDOUT STDERR LOG_LEVEL LOG_NONE LOG_INFO LOG_WARN LOG_VERBOSE LOG_DEBUG
    
    % These integers set type of output from fprintf
    STDOUT = 1; 
    STDERR = 2;
    
    % Distinct integers for each level
    LOG_NONE = 0;
    LOG_WARN = 1;
    LOG_INFO = 2;
    LOG_VERBOSE = 3;
    LOG_DEBUG = 4;
    
    switch level
        case 'none'
            LOG_LEVEL = LOG_NONE;
        case 'warning'
            LOG_LEVEL = LOG_WARN;
        case 'info'
            LOG_LEVEL = LOG_INFO;
        case 'verbose'
            LOG_LEVEL = LOG_VERBOSE;
        case 'debug'
            LOG_LEVEL = LOG_DEBUG;
        otherwise
            error(...
                'logging level must be either ''none'', ''warning'', ''info'', ''verbose'', or ''debug''' ...
            );
    end            
    
end