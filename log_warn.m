function log_warn(string, varargin)
% Logs the given string to the console with timestamp as a warning

    global STDERR LOG_NONE LOG_LEVEL
    
    if isempty(LOG_LEVEL)
        set_log_level('info')
        log_info('No logging level set: using default of ''info''')
    end
    
    if LOG_LEVEL ~= LOG_NONE
        logging_message = ['%s %02d:%02d:%06.3f - WARNING - ', string, '\n'];
        time = clock;
        fprintf(STDERR, logging_message, date, time(4:6), varargin{:});
    end
    
end