% Test logging functions
log_me = 'This text should be logged';
do_not_log_me = 'This text should not be logged';

% Call a logging function without defining a level
log_info(log_me)

% Try and set an invalid logging level, catch and cast to logging function
try
    set_log_level('test')
catch exception
    error_message = getReport(exception);
    log_warn(error_message);
end

fprintf(1, '\n*** Logging set to ''OFF'' ***\n');
set_log_level('none');
log_warn(do_not_log_me);
log_info(do_not_log_me);
log_verbose(do_not_log_me);
log_debug(do_not_log_me);

fprintf(1, '\n*** Logging set to ''WARN'' ***\n');
set_log_level('warning');
log_warn(log_me);
log_info(do_not_log_me);
log_verbose(do_not_log_me);
log_debug(do_not_log_me);

fprintf(1, '\n*** Logging set to ''INFO'' ***\n');
set_log_level('info');
log_warn(log_me);
log_info(log_me);
log_verbose(do_not_log_me);
log_debug(do_not_log_me);


fprintf(1, '\n*** Logging set to ''VERBOSE'' ***\n');
set_log_level('verbose');
log_warn(log_me);
log_info(log_me);
log_verbose(log_me);
log_debug(do_not_log_me);


fprintf(1, '\n*** Logging set to ''DEBUG'' ***\n');
set_log_level('debug');
log_warn(log_me);
log_info(log_me);
log_verbose(log_me);
log_debug(log_me);
