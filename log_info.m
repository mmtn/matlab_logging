function log_info(string, varargin)
% Logs the string to the console with a timestamp

    global STDOUT LOG_INFO LOG_LEVEL
    
    if isempty(LOG_LEVEL)
        set_log_level('info')
        log_info('No logging level set: using default of ''info''')
    end
    
    if LOG_LEVEL >= LOG_INFO
        logging_message = ['%s %02d:%02d:%06.3f - INFO - ', string, '\n'];
        time = clock;
        fprintf(STDOUT, logging_message, date, time(4:6), varargin{:});
    end
    
end