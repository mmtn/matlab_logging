function log_verbose(string, varargin)
% Logs the given string to the console with timestamp

    global STDOUT LOG_VERBOSE LOG_LEVEL
    
    if isempty(LOG_LEVEL)
        set_log_level('info')
        log_info('No logging level set: using default of ''info''')
    end
    
    if LOG_LEVEL >= LOG_VERBOSE
        logging_message = ['%s %02d:%02d:%06.3f - VERBOSE - ', string, '\n'];
        time = clock;
        fprintf(STDOUT, logging_message, date, time(4:6), varargin{:});
    end
    
end