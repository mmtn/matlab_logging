# MATLAB logging functions

### A simple way to add logging functionality to your favourite MATLAB project

#### 1. Get the code

Either downloading it directly from Bitbucket, or run the following in your terminal:

`git clone https://bitbucket.org/mmtn/matlab_logging`

#### 2. Add to MATLAB

In your MATLAB script or function include the line

`addpath('path/to/matlab_logging')`

replacing the path appropriately. You can now use all of the included functions.

#### 3. Usage

Pick a logging level with the `set_log_level` function. The available options in increasing level of detail are `none`, `info`, `verbose`, and `debug`. 

Example: `set_log_level('info')`.

The table below indicates which functions will produce output at different logging levels:

|           | `log_warn` | `log_info` | `log_verbose` | `log_debug` |
|-----------|:----------:|:----------:|:-------------:|:-----------:|
| `none`    |            |            |               |             |
| `warning` |      x     |            |               |             |
| `info`    |      x     |      x     |               |             |
| `verbose` |      x     |      x     |       x       |             |
| `debug`   |      x     |      x     |       x       |      x      |


For instance when the logging level is set to `verbose`, output from `log_info` and `log_verbose` will be displayed, but output from `log_debug` will not be shown.

The `log_warn` function will always be displayed unless the logging level is set to `none`.

#### 4. Extra information

All of the logging functions accept arguments in the same style as the `fprintf` function.
To check the appropriate syntax, type `doc fprintf` in MATLAB and check the `formatSpec` section.
In the first string argument, different types of variables can be represented with `%s` for strings, `%f` for floats, etc.
The names of the variables to be substituted are given as a list after the string (in order of substitution).

For example:
```
string_variable = 'Some text to log';
float_variable = 1.097;
log_info('%s: %6.3f', string_variable, float_variable);
```
