function log_debug(string, varargin)
% Logs the given string to the console with timestamp

    global STDOUT LOG_DEBUG LOG_LEVEL
    
    if isempty(LOG_LEVEL)
        set_log_level('info')
        log_info('No logging level set: using default of ''info''')
    end
    
    if LOG_LEVEL >= LOG_DEBUG
        logging_message = ['%s %02d:%02d:%06.3f - DEBUG - ', string, '\n'];
        time = clock;
        fprintf(STDOUT, logging_message, date, time(4:6), varargin{:});
    end
    
end